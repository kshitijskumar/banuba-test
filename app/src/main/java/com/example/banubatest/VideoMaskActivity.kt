package com.example.banubatest

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.media.AudioManager
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import com.banuba.sdk.effect_player.Effect
import com.banuba.sdk.entity.ContentRatioParams
import com.banuba.sdk.entity.RecordedVideoInfo
import com.banuba.sdk.manager.BanubaSdkManager
import com.banuba.sdk.manager.IEventCallback
import com.banuba.sdk.types.Data
import com.example.banubatest.databinding.ActivityVideoMaskBinding
import java.io.File
import java.lang.Exception

//Activity for recording video
class VideoMaskActivity : AppCompatActivity() {

    companion object {
        private const val TAG = "VideoMaskActivityTag"
    }

    private lateinit var binding: ActivityVideoMaskBinding

    private val permissionsList = mutableListOf<String>()

    private var isRecording = false

    private var currentlyAppliedEffect: Effect? = null

    private var timeStopped = 0L

    private var createdPath: String? = null

    private val permissionResultLauncher =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) {
            it.values.forEach { isGranted ->
                if (!isGranted) {
                    Toast.makeText(this, "Permission not granted", Toast.LENGTH_LONG).show()
                    return@registerForActivityResult
                }
            }
            banubaSdkManager.openCamera()
        }

    private val banubaSdkManager by lazy {
        BanubaSdkManager(applicationContext).apply {
            setCallback(banubaEventCallback)
        }
    }

    private val banubaEventCallback = object : IEventCallback {
        override fun onVideoRecordingFinished(videoInfo: RecordedVideoInfo) {
            Log.d(
                TAG, "Video recording finished. Recorded file = ${videoInfo.filePath}," +
                        "duration = ${videoInfo.recordedLength}"
            )
            createdPath = videoInfo.filePath
        }

        override fun onVideoRecordingStatusChange(isStarted: Boolean) {
            Log.d(TAG, "Video recording status changed. isRecording = $isStarted")
        }

        override fun onCameraOpenError(e: Throwable) {
            // Implement custom error handling here
        }

        override fun onImageProcessed(imageBitmpa: Bitmap) {}

        override fun onEditingModeFaceFound(faceFound: Boolean) {}

        override fun onHQPhotoReady(photoBitmap: Bitmap) {}

        override fun onEditedImageReady(imageBitmap: Bitmap) {}

        override fun onFrameRendered(data: Data, width: Int, height: Int) {}

        override fun onScreenshotReady(screenshotBitmap: Bitmap) {}

        override fun onCameraStatus(isOpen: Boolean) {}
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityVideoMaskBinding.inflate(layoutInflater)
        setContentView(binding.root)


        lowerFilterVolume()

        binding.btnMask1.setOnClickListener {
            handleEffectApply("UnluckyWitch")
        }
        binding.btnMask2.setOnClickListener {
            handleEffectApply("Spider")
        }
        binding.btnMask3.setOnClickListener {
            handleEffectApply("DebugWireframe")
        }
        binding.btnRecord.setOnClickListener {
            if (!isRecording) {
                binding.btnRecord.text = "Stop"
                binding.btnWatch.isEnabled = false
                banubaSdkManager.startVideoRecording(
                    storeLocationPath(), //path where to store
                    true, //whether to record sound too or not. true -> record
                    ContentRatioParams(720, 1280, true),
                    1f // speed for recording
                )
            } else {
                binding.btnRecord.text = "Record"
                banubaSdkManager.effectManager.unload(currentlyAppliedEffect)
                currentlyAppliedEffect = null
                timeStopped = System.currentTimeMillis()
                banubaSdkManager.stopVideoRecording()
            }
            isRecording = !isRecording
        }

        binding.btnWatch.setOnClickListener {
            createdPath?.let { path ->
                openFile(path)
            }
        }
    }

    private fun openFile(path: String) {
        try {
            val file = File(path)
            Log.d(TAG, "File info: $file, ${file.path}, ${file.name}")

            val fileUri = FileProvider.getUriForFile(
                this,
                "com.banuba.sdk.example.effect_player_realtime_preview",
                file
            )
            val intent = Intent(Intent.ACTION_VIEW).apply {
                setDataAndType(fileUri, "video/*")
                addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            }
            startActivity(intent)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    //since filter audio depends on the speaker output, thats why lowering volume myself
    private fun lowerFilterVolume() {
        val audioManager =
            applicationContext.getSystemService(Context.AUDIO_SERVICE) as AudioManager
        val max = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC)
        audioManager.setStreamVolume(
            AudioManager.STREAM_MUSIC,
            max / 2,
            AudioManager.FLAG_PLAY_SOUND
        )
        Log.d(TAG, "Max value is: $max")
    }


    private fun handleEffectApply(maskType: String) {
        try {
            currentlyAppliedEffect?.let {
                banubaSdkManager.effectManager.unload(it)
            }
            currentlyAppliedEffect =
                banubaSdkManager.effectManager.loadAsync(returnMaskUri(maskType).toString())
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun returnMaskUri(maskType: String): Uri {
        Log.d("ResourceBase", "This is: ${BanubaSdkManager.getResourcesBase()}")
        return Uri.parse(BanubaSdkManager.getResourcesBase())
            .buildUpon()
            .appendPath("effects")
            .appendPath(maskType)
            .build()
    }

    override fun onStart() {
        super.onStart()
        banubaSdkManager.attachSurface(binding.surfaceView)
        handlePermissions()
    }

    override fun onResume() {
        super.onResume()
        banubaSdkManager.effectPlayer.playbackPlay()
    }

    override fun onPause() {
        super.onPause()
        banubaSdkManager.effectPlayer.playbackPause()
    }

    override fun onStop() {
        super.onStop()
        Log.d(TAG, "onStop called")
        banubaSdkManager.releaseSurface()
        banubaSdkManager.closeCamera()
    }

    private fun handlePermissions() {
        val cameraPermission = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED
        val audioPermission = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.RECORD_AUDIO
        ) == PackageManager.PERMISSION_GRANTED
        val storagePermission = if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
            ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_GRANTED
        } else true

        if (!cameraPermission) {
            permissionsList.add(Manifest.permission.CAMERA)
        }
        if (!audioPermission) {
            permissionsList.add(Manifest.permission.RECORD_AUDIO)
        }
        if (!storagePermission) {
            permissionsList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }

        if (permissionsList.isEmpty()) {
            banubaSdkManager.openCamera()
        } else {
            permissionResultLauncher.launch(permissionsList.toTypedArray())
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        //call this in onDestroy, memory leak was found. gc root: cameraThread(takes context) and renderThread(no idea)
        banubaSdkManager.recycle()
    }


    private fun storeLocationPath(): String {
        val path = File(
            applicationContext.getExternalFilesDir(Environment.DIRECTORY_MOVIES),
            "banuba_test_${System.currentTimeMillis()}.mp4"
        ).absolutePath
        Log.d(TAG, "try path is: $path")
        return path
    }
}