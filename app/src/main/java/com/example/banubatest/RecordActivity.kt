package com.example.banubatest

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.media.MediaPlayer
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import com.banuba.sdk.effect_player.Effect
import com.banuba.sdk.entity.ContentRatioParams
import com.banuba.sdk.entity.RecordedVideoInfo
import com.banuba.sdk.manager.BanubaSdkManager
import com.banuba.sdk.manager.BanubaSdkTouchListener
import com.banuba.sdk.manager.IEventCallback
import com.banuba.sdk.types.Data
import com.example.banubatest.databinding.ActivityRecordBinding
import kotlinx.coroutines.*
import java.io.File
import java.lang.Exception
import java.util.concurrent.TimeUnit

class RecordActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRecordBinding

    private val permissionsList = mutableListOf<String>()
    private var currentlyAppliedEffect : Effect? = null
    private var isRecording = false

    private val clipsPathList = arrayListOf<String>()
    private var timerCoroutineJob : Job? = null
    private var isMusicSelected = false
    private var isMediaPlayerReady = false
    private var shouldStartWhenPrepared = false

    private var isAnyClipContainsMusic = false

    private var mediaPlayer: MediaPlayer? = null

    private var totalRecordedDuration = 0L

    private val permissionResultLauncher =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) {
            it.values.forEach { isGranted ->
                if (!isGranted) {
                    Toast.makeText(this, "Permission not granted", Toast.LENGTH_LONG).show()
                    return@registerForActivityResult
                }
            }
            banubaSdkManager.openCamera()
        }

    private val banubaSdkManager by lazy {
        BanubaSdkManager(applicationContext).apply {
            setCallback(banubaEventCallback)
        }
    }

    private val banubaEventCallback = object : IEventCallback {
        override fun onCameraOpenError(p0: Throwable) {}

        override fun onCameraStatus(p0: Boolean) {}

        override fun onScreenshotReady(p0: Bitmap) {}

        override fun onHQPhotoReady(p0: Bitmap) {}

        override fun onVideoRecordingFinished(videoInfo: RecordedVideoInfo) {
            Log.d("RecordActivity", "Video info is: location: ${videoInfo.filePath} and duration: ${videoInfo.recordedLength}")
            clipsPathList.add(videoInfo.filePath)
            totalRecordedDuration += videoInfo.recordedLength
        }

        override fun onVideoRecordingStatusChange(isStarted: Boolean) {
            if(isStarted) {
                startRecordingTimer()
            } else {
                stopRecordingTimer()
            }
        }

        override fun onImageProcessed(p0: Bitmap) {}

        override fun onFrameRendered(p0: Data, p1: Int, p2: Int) {}
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRecordBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.surfaceView.setOnTouchListener(BanubaSdkTouchListener(this, banubaSdkManager.effectPlayer))
        applyMask()

        initViews()

    }

    private fun initViews() {
        binding.btnRecordPause.setOnClickListener {
            handleRecordPauseToggle()
        }
        binding.btnDone.setOnClickListener {
            Intent(this, EdittingActivity::class.java).apply {
                putStringArrayListExtra(EdittingActivity.PATHS_LIST, clipsPathList)
                putExtra(EdittingActivity.IS_MUSIC_SELECTED, isAnyClipContainsMusic)
                if (isAnyClipContainsMusic) {
                    putExtra(EdittingActivity.MUSIC_NAME, MUSIC_URL)
                    putExtra(EdittingActivity.TRIM_START, 0L) //demo value
                    putExtra(EdittingActivity.TRIM_END, 10L) //demo value
                }
                startActivity(this)
            }
        }

        binding.btnMusic.setOnClickListener {
            isMusicSelected = !isMusicSelected
            binding.btnMusic.text = if(isMusicSelected) "Remove Music" else "Add Music"
            if (isMusicSelected) {
                mediaPlayer = MediaPlayer().apply {
                    setDataSource(this@RecordActivity, Uri.parse(MUSIC_URL))
                    prepareAsync()
                    setOnPreparedListener {
                        isMediaPlayerReady = true
                        if (shouldStartWhenPrepared) {
                            startRecording(false)
                        }
                        it.start()
                    }
                    isLooping = true
                }
            } else {
                shouldStartWhenPrepared = false
                mediaPlayer?.stop()
            }
        }
    }

    private fun handleRecordPauseToggle() {
        if(isRecording) {
            banubaSdkManager.stopVideoRecording()
            mediaPlayer?.pause()
            binding.btnRecordPause.text = "Record"
            binding.btnDone.isVisible = true
            shouldStartWhenPrepared = false
        } else {
            if(!isMusicSelected) {
                startRecording(true)
            } else {
                isAnyClipContainsMusic = true
                if(totalRecordedDuration != 0L) {
                    mediaPlayer?.start()
                    startRecording(false)
                    isRecording = !isRecording
                    return
                }
                if(isMediaPlayerReady) {
                    mediaPlayer?.pause()
                    mediaPlayer?.seekTo(0)
                    mediaPlayer?.setOnSeekCompleteListener {
                        it?.start()
                        startRecording(false)
                    }
                } else {
                    shouldStartWhenPrepared = true
                }
            }
        }
        isRecording = !isRecording
    }
    private fun startRecording(micStatus: Boolean) {
        banubaSdkManager.startVideoRecording(
            storeLocationPath(),
            micStatus,
            ContentRatioParams(720, 1280, true),
            1f
        )
        binding.btnRecordPause.text = "Pause"
        binding.btnDone.isVisible = false
        binding.btnMusic.isVisible = false
    }

    private fun applyMask() {
        try {
            currentlyAppliedEffect?.let {
                banubaSdkManager.effectManager.unload(it)
            }
            currentlyAppliedEffect =
                banubaSdkManager.effectManager.loadAsync(returnMaskUri("UnluckyWitch").toString())
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun returnMaskUri(maskType: String): Uri {
        Log.d("ResourceBase", "This is: ${BanubaSdkManager.getResourcesBase()}")
        return Uri.parse(BanubaSdkManager.getResourcesBase())
            .buildUpon()
            .appendPath("effects")
            .appendPath(maskType)
            .build()
    }

    private fun storeLocationPath(): String {
        return File(
            applicationContext.getExternalFilesDir(Environment.DIRECTORY_MOVIES),
            "banuba_test_${System.currentTimeMillis()}.mp4"
        ).absolutePath
    }

    private fun startRecordingTimer() {
        timerCoroutineJob = lifecycleScope.launch(Dispatchers.IO) {
            withContext(Dispatchers.Main) {
                if(totalRecordedDuration == 0L) binding.tvTimer.text = "00 : 00"
            }
            var startLengthOfThisClip = totalRecordedDuration
            while (true) {
                delay(950)
                withContext(Dispatchers.Main) {
                    startLengthOfThisClip += 1000
                    updateTimerText(startLengthOfThisClip)
                }
            }

        }
    }

    private fun stopRecordingTimer() {
        timerCoroutineJob?.cancel()
        timerCoroutineJob = null
    }

    private fun updateTimerText(timeElapsed: Long) {
        var millis = timeElapsed
        val hours = TimeUnit.MILLISECONDS.toHours(millis)
        millis -= TimeUnit.HOURS.toMillis(hours)
        val minutes = TimeUnit.MILLISECONDS.toMinutes(millis)
        millis -= TimeUnit.MINUTES.toMillis(minutes)
        val seconds = TimeUnit.MILLISECONDS.toSeconds(millis)
        val timeText = "${if (minutes < 10) "0" else ""}$minutes : ${if (seconds < 10) "0" else ""}$seconds"
        binding.tvTimer.text = timeText
    }

    override fun onStart() {
        super.onStart()
        banubaSdkManager.attachSurface(binding.surfaceView)
        handlePermissions()
    }

    override fun onResume() {
        super.onResume()
        banubaSdkManager.effectPlayer.playbackPlay()
    }

    override fun onPause() {
        super.onPause()
        banubaSdkManager.effectPlayer.playbackPause()
    }

    override fun onStop() {
        super.onStop()
        banubaSdkManager.releaseSurface()
        banubaSdkManager.closeCamera()
        mediaPlayer?.stop()
        mediaPlayer?.release()
        mediaPlayer = null
    }

    override fun onDestroy() {
        super.onDestroy()
        banubaSdkManager.recycle()
    }

    private fun handlePermissions() {
        val cameraPermission = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED
        val audioPermission = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.RECORD_AUDIO
        ) == PackageManager.PERMISSION_GRANTED
        val storagePermission = if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
            ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_GRANTED
        } else true

        if (!cameraPermission) {
            permissionsList.add(Manifest.permission.CAMERA)
        }
        if (!audioPermission) {
            permissionsList.add(Manifest.permission.RECORD_AUDIO)
        }
        if (!storagePermission) {
            permissionsList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }

        if (permissionsList.isEmpty()) {
            banubaSdkManager.openCamera()
        } else {
            permissionResultLauncher.launch(permissionsList.toTypedArray())
        }
    }

    companion object {
        private const val MUSIC_URL = "https://firebasestorage.googleapis.com/v0/b/learning-firebase-f7960.appspot.com/o/sasageyo_lofi.mp3?alt=media&token=719f0afd-1981-40ab-99ff-c7c4d7cfc795"
    }
}