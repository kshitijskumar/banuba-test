package com.example.banubatest

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.banubatest.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnPreview.setOnClickListener {
            Intent(this, CameraPreviewActivity::class.java).also {
                startActivity(it)
            }
        }

        binding.btnMask.setOnClickListener {
            Intent(this, MaskActivity::class.java).also {
                startActivity(it)
            }
        }
        binding.btnVideoMask.setOnClickListener {
            Intent(this, VideoMaskActivity::class.java).also {
                startActivity(it)
            }
        }
        binding.btnRecorder.setOnClickListener {
            Intent(this, RecordActivity::class.java).also {
                startActivity(it)
            }
        }
    }
}