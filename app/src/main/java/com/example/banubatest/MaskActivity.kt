package com.example.banubatest

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.hardware.camera2.CameraManager
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.SeekBar
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import com.banuba.sdk.camera.Facing
import com.banuba.sdk.effect_player.Effect
import com.banuba.sdk.manager.BanubaSdkManager
import com.banuba.sdk.manager.BanubaSdkTouchListener
import com.example.banubatest.databinding.ActivityMaskBinding
import java.lang.Exception

//Activity to preview filters
class MaskActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMaskBinding

    //same as that of folders for these filters in assets
    companion object {
        const val UNLUCKY_WITCH = "UnluckyWitch"
        const val DEBUG_WIREFRAME = "DebugWireframe"
        const val SPIDER = "Spider"
        const val CUBEMAP_EVEREST = "CubemapEverest"
        const val HAIR_SEGMENTATION = "HairSegmentation"
        const val EYE_TEST = "TestEyes"
        const val SKIN_TEST = "TestSkin"
    }

    private val requestPermissionLauncher = registerForActivityResult(ActivityResultContracts.RequestPermission()) {
        if(it) {
            banubaSdkManager.openCamera()
        }else {
            Toast.makeText(this, "permission denied", Toast.LENGTH_LONG)
        }
    }

    private val banubaSdkManager by lazy {
        BanubaSdkManager(applicationContext)
    }

    private var currentlyAppliedEffect: Effect? = null
    private var isFlashOn = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMaskBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // for any kind of touch interaction effect that filter have
        binding.surfaceView.setOnTouchListener(BanubaSdkTouchListener(this, banubaSdkManager.effectPlayer))

        binding.btnMask1.setOnClickListener {
            handleEffectApply(UNLUCKY_WITCH)
        }
        binding.btnMask2.setOnClickListener {
            handleEffectApply(HAIR_SEGMENTATION)
        }
        binding.btnMask3.setOnClickListener {
            handleEffectApply(DEBUG_WIREFRAME)
        }
        binding.btnSwitchFace.setOnClickListener {
            toggleCameraPosition()
        }
        binding.btnNoMask.setOnClickListener {
            banubaSdkManager.effectManager.unload(currentlyAppliedEffect)
            currentlyAppliedEffect = null
        }
        binding.btnTriggerSpider.setOnClickListener {
            handleEffectApply(SPIDER)
            Toast.makeText(this, "Open your mouth", Toast.LENGTH_LONG).show()
        }
        binding.btnBgSeg.setOnClickListener {
            handleEffectApply(CUBEMAP_EVEREST)
        }
        binding.btnEyeSeg.setOnClickListener {
            handleEffectApply(EYE_TEST)
        }
        binding.btnSkinSeg.setOnClickListener {
            handleEffectApply(SKIN_TEST)
        }
        binding.btnSlime.setOnClickListener {
            Toast.makeText(this, "Touch the screen now", Toast.LENGTH_LONG).show()
            handleEffectApply("ASMRSlime")
        }
        binding.btnBlur2.setOnClickListener {
            handleEffectApply("blur_bg_2")
        }
        binding.btnEyelashes.setOnClickListener {
            handleEffectApply("test_Eyelashes")
        }
        binding.btnLips.setOnClickListener {
            handleEffectApply("test_Lips_shine")
        }
        binding.btnLipstick.setOnClickListener {
            handleEffectApply("Makeup_Lips_Shiny_default")
            currentlyAppliedEffect?.callJsMethod("Lips.shiny", "1 0 0.49 1")
            Toast.makeText(this, "random values for now. but customizable", Toast.LENGTH_LONG).show()
        }
        binding.btnFlash.setOnClickListener {
            toggleFlash()
        }


        binding.sbZoom.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                Log.d("SeekbarProgess", "Progress: $progress")
                if(fromUser) {
                    banubaSdkManager.setCameraZoom((progress+1).toFloat())
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) = Unit

            override fun onStopTrackingTouch(seekBar: SeekBar?) = Unit
        })
    }

    private fun handleEffectApply(maskType: String) {
        try {
            currentlyAppliedEffect?.let {
                banubaSdkManager.effectManager.unload(it)
            }
            currentlyAppliedEffect = banubaSdkManager.effectManager.loadAsync(returnMaskUri(maskType).toString())
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    //logic not working for flash
    private fun toggleFlash() {
        if(!isFlashFeatureAvailable()) {
            Toast.makeText(this, "flash not available", Toast.LENGTH_SHORT).show()
            return
        }
        val cameraManager = getSystemService(Context.CAMERA_SERVICE) as CameraManager
        try {
            Log.d("CameraList", "the list is: ${cameraManager.cameraIdList}")
            val cameraId = cameraManager.cameraIdList[0]
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                cameraManager.setTorchMode(cameraId, !isFlashOn)
                isFlashOn = !isFlashOn
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun isFlashFeatureAvailable(): Boolean {
        return applicationContext.packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)
    }

    private fun returnMaskUri(maskType: String) : Uri {
        Log.d("ResourceBase", "This is: ${BanubaSdkManager.getResourcesBase()}")
        return Uri.parse(BanubaSdkManager.getResourcesBase())
            .buildUpon()
            .appendPath("effects")
            .appendPath(maskType)
            .build()
    }


    private fun toggleCameraPosition() {
        banubaSdkManager.cameraFacing = when(banubaSdkManager.cameraFacing) {
            Facing.BACK -> {
                banubaSdkManager.setRequireMirroring(true)
                Facing.FRONT
            }
            Facing.FRONT -> {
                banubaSdkManager.setRequireMirroring(false)
                Facing.BACK
            }
            else -> Facing.FRONT
        }
    }

    override fun onStart() {
        super.onStart()
        banubaSdkManager.attachSurface(binding.surfaceView)

        handlePermissions()
    }

    override fun onResume() {
        super.onResume()
        banubaSdkManager.effectPlayer.playbackPlay()
    }

    override fun onPause() {
        super.onPause()
        banubaSdkManager.effectPlayer.playbackPause()
    }

    override fun onStop() {
        super.onStop()
        banubaSdkManager.releaseSurface()
        banubaSdkManager.closeCamera()
    }

    private fun handlePermissions() {
        val hasPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
        if(hasPermission) {
            banubaSdkManager.openCamera()
        }else {
            requestPermissionLauncher.launch(Manifest.permission.CAMERA)
        }
    }
}