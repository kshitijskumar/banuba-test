package com.example.banubatest

import android.app.Application
import com.banuba.sdk.manager.BanubaSdkManager

class TestApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        BanubaSdkManager.initialize(this, BanubaClientToken.KEY)
    }
}