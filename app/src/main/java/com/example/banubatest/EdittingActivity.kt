package com.example.banubatest

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.content.FileProvider
import com.example.banubatest.databinding.ActivityEdittingBinding
import java.io.File
import java.lang.Exception
import java.lang.StringBuilder

class EdittingActivity : AppCompatActivity() {

    private lateinit var binding: ActivityEdittingBinding

    private val _pathsList = mutableListOf<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEdittingBinding.inflate(layoutInflater)
        setContentView(binding.root)

        getInformationFromIntent()

        binding.btnWatch.setOnClickListener {
            try {
                val clipIndex = binding.etClipNum.text.toString().toInt()
                if (clipIndex < _pathsList.size) {
                    openFile(_pathsList[clipIndex])
                } else {
                    throw ArrayIndexOutOfBoundsException("Enter a valid number")
                }
            } catch (e: Exception) {
                e.printStackTrace()
                Toast.makeText(this, e.message, Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun getInformationFromIntent() {
        val pathList = intent.getStringArrayListExtra(PATHS_LIST) ?: arrayListOf()
        pathList.forEach { _pathsList.add(it) }
        updateViews(pathList)
        updateMusicRelatedInfo()
    }

    private fun updateViews(pathList: ArrayList<String>) {
        binding.tvClipsCount.text = "No. of clips = ${pathList.size}"
        val pathsListString = StringBuilder("Paths: \n")
        pathList.forEachIndexed { index, path ->
            pathsListString.append("$index. $path \n")
        }
        binding.tvClipsPaths.text = pathsListString
    }

    private fun updateMusicRelatedInfo() {
        val isAnyClipContainsMusic = intent.getBooleanExtra(IS_MUSIC_SELECTED, false)
        if (isAnyClipContainsMusic) {
            binding.tvMusicDetails.visibility = View.VISIBLE
            binding.tvMusicMsg.visibility = View.VISIBLE

            val musicDetailsStringBuilder = StringBuilder("Music Details: \n")
            musicDetailsStringBuilder.append("File: ${intent.getStringExtra(MUSIC_NAME)} \n")
            musicDetailsStringBuilder.append("trim start (demo value): ${intent.getLongExtra(TRIM_START, 0L)} \n")
            musicDetailsStringBuilder.append("trim end (demo value): ${intent.getLongExtra(TRIM_END, 0L)} \n")

            binding.tvMusicDetails.text = musicDetailsStringBuilder
        }
    }

    private fun openFile(path: String) {
        try {
            val file = File(path)

            val fileUri = FileProvider.getUriForFile(
                this,
                "com.banuba.sdk.example.effect_player_realtime_preview",
                file
            )
            val intent = Intent(Intent.ACTION_VIEW).apply {
                setDataAndType(fileUri, "video/*")
                addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            }
            startActivity(intent)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    companion object {
        const val PATHS_LIST = "PATH_LIST"
        const val IS_MUSIC_SELECTED = "IS_MUSIC_SELECTED"
        const val MUSIC_NAME = "MUSIC_NAME"
        const val TRIM_START = "TRIM_START"
        const val TRIM_END = "TRIM_END"
    }
}