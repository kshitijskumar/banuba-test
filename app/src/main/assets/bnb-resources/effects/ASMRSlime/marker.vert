#version 300 es

const float marker_size = 750.;

precision mediump sampler2DArray;

layout( location = 0 ) in vec3 attrib_pos;
layout( location = 1 ) in vec3 attrib_n;
layout( location = 2 ) in vec4 attrib_t;
layout( location = 3 ) in vec2 attrib_uv;
layout( location = 4 ) in uvec4 attrib_bones;
layout( location = 5 ) in vec4 attrib_weights;

layout(std140) uniform glfx_GLOBAL
{
	mat4 glfx_MVP;
	mat4 glfx_PROJ;
	mat4 glfx_MV;
	vec4 glfx_QUAT;
	vec4 touch[6];
};
layout(std140) uniform glfx_BASIS_DATA
{
	vec4 unused;
	vec4 glfx_SCREEN;
};

uniform uint glfx_CURRENT_I;

out vec2 var_uv;
out vec2 var_dir;

void main()
{
	vec2 marker_end = (touch[glfx_CURRENT_I].xy*0.5+0.5)*glfx_SCREEN.xy;
	vec2 marker_start = (touch[glfx_CURRENT_I].zw*0.5+0.5)*glfx_SCREEN.xy;

	vec2 dir = marker_end-marker_start;
	float ldir = length(dir);
	vec2 ndir = ldir > 0. ? dir/ldir : vec2(1.,0.);

	vec2 marker_pos = (marker_start + marker_end)*0.5;

	vec2 v = attrib_pos.xy;
	v.x *= 1. + ldir/marker_size;

	v = mat2(ndir, vec2(-ndir.y, ndir.x))*v;

	vec2 vpos = marker_pos + v*(marker_size*0.5);

	gl_Position = vec4(vpos*glfx_SCREEN.zw*2.-1.,0.,1.);

	var_uv = attrib_pos.xy;
	var_dir = -dir*glfx_SCREEN.z;
}
