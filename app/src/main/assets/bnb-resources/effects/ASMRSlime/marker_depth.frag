#version 300 es

precision highp float;

const float falloff = 2.; // higher value -> sharper area
const float strength = 0.125;

layout( location = 0 ) out vec4 F;

in vec2 var_uv;

void main()
{
	float r = 1.-min(length(var_uv),1.);
	float magnitude = pow(smoothstep(0.,1.,r),falloff)*strength;
	F = vec4(magnitude,magnitude,magnitude,1.);
}
