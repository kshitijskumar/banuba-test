Настойки:

1. Источник(и) света в slimedraw.frag:
// light position in xyz, radiance in w
const vec4 lights[] = vec4[]( 
	//vec4(0.,2.6,2.8, 0.5),
	vec4(0.46,-1.73,9.86, 3.0)
	);

сейчас один точечный с позицией xyz 0.46,-1.73,9.86 и яркостью 3.0. можно раскоментировать еще один или добавить других аналогичным образом.


2. Область тача для продавливания вглубь: 


В marker_depth.vert

размер области:
const float marker_size = 350.;


В marker_depth.frag

"жёсткость" области, чем выше значение, тем более "сфокусированной" будет область:
const float falloff = 2.; // higher value -> sharper area

сила(скорость) продавливания:
const float strength = 0.125;


3. Область тача для сдвига:


В marker.vert

размер области:
const float marker_size = 750.;


В marker.frag

"жёсткость" области, чем выше значение, тем более "сфокусированной" будет область:
const float falloff = 1.5; // higher value -> sharper area

сила сдвига:
const float strength = 1.0;


4. Симуляция жижи:


В simulate.frag

сила, с которой сдвинутая жижа стремиться вернуться назад:
const float force = -12.;

затухание колебаний, чем ближе к 1.0, тем дольше болтается туда-сюда:
const float damping = 0.94;


В fade_depth.frag

скорость восстановления продавливаний, чем выше, тем быстрее:
const float depth_fade = 0.03;