#version 300 es

precision mediump float;

in vec2 var_uv;

layout( location = 0 ) out vec4 F;

uniform sampler2D sim_mask;

void main()
{
	F = texture( sim_mask, var_uv );
}
