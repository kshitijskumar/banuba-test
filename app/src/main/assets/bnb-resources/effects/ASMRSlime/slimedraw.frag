#version 300 es

precision highp float;

// light position in xyz, radiance in w
const vec4 lights[] = vec4[]( 
	//vec4(0.,2.6,2.8, 0.5),
	vec4(0.46,-1.73,9.86, 3.0)
	);

in vec2 var_uv;

layout( location = 0 ) out vec4 frag_color;

uniform sampler2D bg_image;
uniform sampler2D sim_mask;
uniform sampler2D depth_mask;

layout(std140) uniform glfx_BASIS_DATA
{
	highp vec4 unused;
	// framebuffer width, height, 1/width, 1/height
	highp vec4 glfx_SCREEN;
};

uniform sampler2D tex_base;
uniform sampler2D tex_normal;
uniform sampler2D tex_base2;
uniform sampler2D tex_normal2;

uniform sampler2D tex_metallic;
uniform sampler2D tex_roughness;

// gamma to linear
vec3 g2l( vec3 g )
{
	return g*(g*(g*0.305306011+0.682171111)+0.012522878);
}

// combined hdr to ldr and linear to gamma
vec3 l2g( vec3 l )
{
	return sqrt(1.33*(1.-exp(-l)))-0.03;
}

vec3 fresnel_schlick( float prod, vec3 F0 )
{
	return F0 + ( 1. - F0 )*pow( 1. - prod, 5. );
}

float distribution_GGX( float cN_H, float roughness )
{
	float a = roughness*roughness;
	float a2 = a*a;
	float d = cN_H*cN_H*( a2 - 1. ) + 1.;
	return a2/(3.14159265*d*d);
}

float geometry_schlick_GGX( float NV, float roughness )
{
	float r = roughness + 1.;
	float k = r*r/8.;
	return NV/( NV*( 1. - k ) + k );
}

float geometry_smith( float cN_L, float ggx2, float roughness )
{
	return geometry_schlick_GGX( cN_L, roughness )*ggx2;
}

void main()
{
	float depth = texture( depth_mask, var_uv.xy ).x;

	float dl = texture( depth_mask, vec2(var_uv.x-1.5/180.,var_uv.y) ).x;
	float dr = texture( depth_mask, vec2(var_uv.x+1.5/180.,var_uv.y) ).x;
	float db = texture( depth_mask, vec2(var_uv.x,var_uv.y-1.5/320.) ).x;
	float dt = texture( depth_mask, vec2(var_uv.x,var_uv.y+1.5/320.) ).x;

	vec2 uv_off = texture( sim_mask, var_uv.xy ).xy;
	vec2 uv = var_uv + uv_off;
	vec2 tex_sz = vec2(textureSize(tex_base,0));
	uv.x *= glfx_SCREEN.x*glfx_SCREEN.w*(tex_sz.y/tex_sz.x);

	vec3 base = g2l(texture(tex_base,uv).xyz);
	float metallic = texture(tex_metallic,uv).x;
	float roughness = texture(tex_roughness,uv).x;
	vec3 N = texture(tex_normal,uv).xyz*2.-1.;
	N.xy += vec2(dr-dl,dt-db)*20.;
	N = normalize(N);

	vec3 V = vec3( 0., 0., 1. );
	float cN_V = max( 0., dot( N, V ) );
	vec3 R = reflect( -V, N );

	vec3 F0 = mix( vec3(0.04), base, metallic );

	vec3 color = 0.03*base; // ambient

	float ao = exp2(-depth);

	float ggx2 = geometry_schlick_GGX( cN_V, roughness );
	for( int i = 0; i != lights.length(); ++i )
	{
		vec4 lw = lights[i];
		lw.xy -= (var_uv*2.-1.)*vec2(7.*glfx_SCREEN.x*glfx_SCREEN.w,7.);
		vec3 L = normalize(lw.xyz);
		vec3 H = normalize( V + L );
		float N_L = dot( N, L );
		float cN_L = max( 0., N_L );
		float cN_H = max( 0., dot( N, H ) );
		float cH_V = max( 0., dot( H, V ) );

		float NDF = distribution_GGX( cN_H, roughness );
		float G = geometry_smith( cN_L, ggx2, roughness );
		vec3 F_light = fresnel_schlick( cH_V, F0 );

		vec3 specular = NDF*G*F_light/( 4.*cN_V*cN_L + 0.001 );

		vec3 kD_light = ( 1. - F_light )*( 1. - metallic );

		color += ( kD_light*base/3.14159265*ao + specular )*lw.w*cN_L;
	}

	frag_color = vec4(l2g(color),1.);
}
