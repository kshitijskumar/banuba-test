
function Effect()
{
	var self = this;

	// this.play = function() {
	// 	var now = (new Date()).getTime();
	// };

	this.init = function() {
		Api.meshfxMsg("spawn", 0, 0, "quad.bsm2");
		Api.meshfxMsg("spawn", 1, 0, "quad.bsm2");
		Api.meshfxMsg("spawn", 2, 0, "quad.bsm2");
		Api.meshfxMsg("spawn", 3, 0, "quad.bsm2");
		Api.meshfxMsg("spawn", 4, 0, "quad.bsm2");
		Api.meshfxMsg("spawn", 5, 0, "quad.bsm2");

		//Api.meshfxMsg("shaderVec4", 0, 0, "0.5 0.5 0.6 0.3");
		Api.meshfxMsg("shaderVec4", 0, 0, "2 2 2 2");
		Api.meshfxMsg("shaderVec4", 0, 1, "2 2 2 2");
		Api.meshfxMsg("shaderVec4", 0, 2, "2 2 2 2");
		Api.meshfxMsg("shaderVec4", 0, 3, "2 2 2 2");
		Api.meshfxMsg("shaderVec4", 0, 4, "2 2 2 2");
		Api.meshfxMsg("shaderVec4", 0, 5, "2 2 2 2");

		Api.meshfxMsg("spawn", 6, 0, "tri.bsm2");

		Api.showRecordButton();
	};

	this.restart = function() {
		Api.meshfxReset();
		self.init();
	};

	this.faceActions = [];
	this.noFaceActions = [];

	this.videoRecordStartActions = [this.restart];
	this.videoRecordFinishActions = [];
	this.videoRecordDiscardActions = [this.restart];
}

configure(new Effect());

var touch_ids = [undefined,undefined,undefined,undefined,undefined,undefined];
var prev_touches = [undefined,undefined,undefined,undefined,undefined,undefined];

function onTouchesBegan( touches )
{
	for( var i = 0; i < touches.length; i++ )
	{
		var touch_idx = touch_ids.indexOf(undefined);
		if( touch_idx != -1 )
		{
			touch_ids[touch_idx] = touches[i].id;
			prev_touches[touch_idx] = {x:touches[i].x,y:touches[i].y};
			Api.meshfxMsg("shaderVec4", 0, touch_idx, touches[i].x + " " + touches[i].y 
				+ " " + touches[i].x + " " + touches[i].y);
		}
	}
}

function onTouchesEnded( touches )
{
	for( var i = 0; i < touches.length; i++ )
	{
		var touch_idx = touch_ids.indexOf(touches[i].id);
		if( touch_idx != -1 )
		{
			touch_ids[touch_idx] = undefined;
			prev_touches[touch_idx] = undefined;
			Api.meshfxMsg("shaderVec4", 0, touch_idx, "2 2 2 2");
		}
	}
}

function onTouchesCancelled()
{
	touch_ids = [undefined,undefined,undefined,undefined,undefined,undefined];
	prev_touches = [undefined,undefined,undefined,undefined,undefined,undefined];
	Api.meshfxMsg("shaderVec4", 0, 0, "2 2 2 2");
	Api.meshfxMsg("shaderVec4", 0, 1, "2 2 2 2");
	Api.meshfxMsg("shaderVec4", 0, 2, "2 2 2 2");
	Api.meshfxMsg("shaderVec4", 0, 3, "2 2 2 2");
	Api.meshfxMsg("shaderVec4", 0, 4, "2 2 2 2");
	Api.meshfxMsg("shaderVec4", 0, 5, "2 2 2 2");
}

function onTouchesMoved( touches )
{
	for( var i = 0; i < touches.length; i++ )
	{
		var touch_idx = touch_ids.indexOf(touches[i].id);
		if( touch_idx != -1 )
		{
			Api.meshfxMsg("shaderVec4", 0, touch_idx, touches[i].x + " " + touches[i].y
				+ " " + prev_touches[touch_idx].x + " " +prev_touches[touch_idx].y);
			prev_touches[touch_idx] = {x:touches[i].x,y:touches[i].y};
		}
	}
}
