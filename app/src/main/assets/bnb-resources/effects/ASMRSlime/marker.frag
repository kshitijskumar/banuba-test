#version 300 es

precision highp float;

const float falloff = 1.5; // higher value -> sharper area
const float strength = 1.0;

layout( location = 0 ) out vec4 F;

in vec2 var_uv;

in vec2 var_dir;

void main()
{
	float r = 1.-min(length(var_uv),1.);
	float magnitude = pow(smoothstep(0.,1.,r),falloff)*strength;

	vec2 d = var_dir*magnitude;

	F = vec4(d,d);
}
