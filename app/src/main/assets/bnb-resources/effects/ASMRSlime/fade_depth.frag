#version 300 es

precision highp float;

const float depth_fade = 0.03;

layout( location = 0 ) out vec4 F;

void main()
{
	F = vec4(0.,0.,0.,depth_fade);
}
