#version 300 es

precision highp float;

const float force = -12.;
const float damping = 0.94;

layout( location = 0 ) out vec4 F;

uniform sampler2D paint_mask;

in vec2 var_uv;

void main()
{
	vec4 old = texture( paint_mask, var_uv );

	vec2 p1 = var_uv + old.xy;
	vec2 p2 = var_uv + old.zw;
	const float dt2 = (1./30.)*(1./30.);
	const float fdt2 = force*dt2;
	vec2 acc = old.xy*fdt2;
	vec2 p0 = p1 + (p1-p2)*damping + acc;
	F = vec4( p0 - var_uv, p1 - var_uv );
}
