#version 300 es
precision highp float;

in vec2 vTexCoord;
in vec2 vTexCoordMask;

uniform sampler2D samplerCamera;
uniform sampler2D samplerMask;
uniform int channel;
uniform float aspect;

out vec4 glFragColor;


const float kernel[] = float[](
0.53940751,
0.60653066,
0.67363846,
0.73899130,
0.80073740,
0.85699689,
0.90595519,
0.94595947,
0.97561098,
0.99384617,
1.0,
0.99384617,
0.97561098,
0.94595947,
0.90595519,
0.85699689,
0.8007374,
0.7389913,
0.67363846,
0.60653066,
0.53940751
);

uniform vec2 wh;
uniform vec2 dir;

vec4 getColor(vec2 tex) {
    return texture(samplerCamera, tex);
}

vec2 getTextCoord(vec2 vTexCoord, float shift) {
    return vec2(vTexCoord.x + dir.x * shift / wh.x, vTexCoord.y + dir.y * shift / wh.y);
}


vec4 makeBlurMask(vec2 vTexCoord, vec2 vTexMask) {

    vec4 sum = vec4(0.0);
    vec4 w_sum = vec4(0.0);

    float sigma_r = 0.01;
    float range_factor = sigma_r * sqrt(2.0);


    for (int a = 0; a < kernel.length(); a++) {
        int i = a - int(floor(float(kernel.length()) / 2.0));
        float w_s = kernel[a];

        vec2 vTexMaskI = vTexMask + float(i) * dir  / wh;
        vec2 vTexCoordI = vTexCoord + float(i) * dir  / wh;

        float mask = texture(samplerMask, vTexMaskI)[channel];
        vec4 color = texture(samplerCamera, vTexCoordI);

        float w_r = mask;
        if (i==0) {
            w_r =  1.0 / max(1.0 - mask, 0.1);
        } else {
            w_r = exp(-w_r*w_r/range_factor);
        }
        float w_total = w_s*w_r;
        w_sum += w_total;
        sum += w_total*color;
    }

    vec4 result = sum / w_sum;

    return vec4(result.rgb, 1.0);


}

void main() {

    // http://dev.theomader.com/gaussian-kernel-calculator/


    vec4 mask4 = texture(samplerMask, vTexCoordMask);
    float mask = mask4[channel];


    if (mask > 0.95) {
        glFragColor = getColor(vTexCoord);
    } else {
        glFragColor = makeBlurMask(vTexCoord, vTexCoordMask);
    }

}