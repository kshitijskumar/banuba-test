#version 300 es

precision lowp float;

in vec2 var_uv;

layout( location = 0 ) out vec4 F;

uniform sampler2D tex_bg_masked;

layout(std140) uniform glfx_BASIS_DATA
{
	highp vec4 unused;
	highp vec4 glfx_SCREEN;
	highp vec4 glfx_BG_MASK_T[2];
	highp vec4 glfx_HAIR_MASK_T[2];
	highp vec4 glfx_LIPS_MASK_T[2];
	highp vec4 glfx_L_EYE_MASK_T[2];
	highp vec4 glfx_R_EYE_MASK_T[2];
	highp vec4 glfx_SKIN_MASK_T[2];
	highp vec4 glfx_OCCLUSION_MASK_T[2];
};

const float kernel[] = float[](
0.53940751,
0.60653066,
0.67363846,
0.73899130,
0.80073740,
0.85699689,
0.90595519,
0.94595947,
0.97561098,
0.99384617,
1.0,
0.99384617,
0.97561098,
0.94595947,
0.90595519,
0.85699689,
0.8007374,
0.7389913,
0.67363846,
0.60653066,
0.53940751
);
const vec2 dir = vec2(1.,0.);

vec3 makeBlurMask(vec2 vTexCoord) {

	vec4 sum = vec4(0.0);
	vec4 w_sum = vec4(0.0);

	float sigma_r = 0.01;
	float range_factor = sigma_r * sqrt(2.0);

	for (int a = 0; a < kernel.length(); a++) {
		int i = a - int(floor(float(kernel.length()) / 2.0));
		float w_s = kernel[a];

		vec2 vTexCoordI = vTexCoord + float(i) * dir * glfx_SCREEN.zw;

		vec4 color = texture(tex_bg_masked, vTexCoordI);
		float mask = color.w;

		float w_r = mask;
		if (i==0) {
			w_r =  1.0 / max(1.0 - mask, 0.1);
		} else {
			w_r = exp(-w_r*w_r/range_factor);
		}
		float w_total = w_s*w_r;
		w_sum += w_total;
		sum += w_total*color;
	}

	vec4 result = sum / w_sum;

	return result.rgb;
}

void main()
{
	vec4 c = texture(tex_bg_masked, var_uv);

	if (c.w > 0.95) {
		F = c;
	} else {
		F = vec4(makeBlurMask(var_uv),c.w);
	}
}
