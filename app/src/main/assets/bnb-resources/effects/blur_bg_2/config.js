function Effect() {
    var self = this;

    this.init = function() {
        Api.meshfxMsg("blur", 0, 4);
        Api.meshfxMsg("spawn", 0, 0, "tri.bsm2");
        Api.showRecordButton();
    };

    this.restart = function() {
        Api.meshfxReset();
        self.init();
    };

    this.faceActions = [];
    this.noFaceActions = [];

    this.videoRecordStartActions = [];
    this.videoRecordFinishActions = [];
    this.videoRecordDiscardActions = [this.restart];
}

configure(new Effect());