var rot = 1.0;

function Effect()
{
    var self = this;

    this.init = function() {
        Api.meshfxMsg("wlut", 1, 50, "lut/lut3d_eyes_verylow.png");
        Api.meshfxMsg("wlut", 2, 100, "lut/lut3d_teeth_highlighter5.png");
        Api.meshfxMsg("shaderVec4", 0, 0, "0 0 1");
        Api.meshfxMsg("spawn", 0, 0, "!glfx_FACE");
        Api.showRecordButton();
    };

    this.restart = function() {
        Api.meshfxReset();
        self.init();
    };

    this.faceActions = [track_angle];
    this.noFaceActions = [];

    this.videoRecordStartActions = [];
    this.videoRecordFinishActions = [];
    this.videoRecordDiscardActions = [];
}

var effect = new Effect();

function track_angle() {
    var mv = Api.modelview();
    rot = Math.min(Math.max((mv[6] + 0.15) / (0.15 - 0.01), 0.0), 1.0);
    var trackedValue = "0 0 " + rot;
    Api.meshfxMsg("shaderVec4", 0, 0, trackedValue);
}

configure(effect);