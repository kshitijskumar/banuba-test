#version 300 es

precision mediump float;

in vec2 var_uv;
flat in float step_y;

layout( location = 0 ) out vec4 F;

uniform sampler2D bokeh_hR;
uniform sampler2D bokeh_hG;
uniform sampler2D bokeh_hB;
uniform sampler2D bg_masked;

const vec4 kernel[] = vec4[](
	vec4(0.064754,0.000000,  0.064754,0.000000),
	vec4(0.063647,0.005252,  0.062782,0.001529),
	vec4(0.057972,0.019812,  0.057015,0.005570),
	vec4(0.042178,0.038585,  0.047976,0.010684),
	vec4(0.013015,0.050223,  0.036693,0.015064),
	vec4(-0.021449,0.040468, 0.024700,0.017215),
	vec4(-0.038708,0.006957, 0.013753,0.016519),
	vec4(-0.020612,-0.025574,0.005324,0.013416),
	vec4(0.014096,-0.022658, 0.000115,0.009116)
);

const int KERNEL_RADIUS = 8;

vec2 mul_complex( vec2 p, vec2 q )
{
	return vec2( p.x*q.x - p.y*q.y, p.x*q.y + p.y*q.x );
}

void main()
{
	vec4 valR = vec4(0.);
	vec4 valG = vec4(0.);
	vec4 valB = vec4(0.);

	for( int i = -KERNEL_RADIUS; i <= KERNEL_RADIUS; ++i )
	{
		vec2 uv = var_uv;
		uv.y + step_y*float(i);
		vec4 imgR = texture(bokeh_hR,uv);
		vec4 imgG = texture(bokeh_hR,uv);
		vec4 imgB = texture(bokeh_hR,uv);
		vec4 k = kernel[abs(i)];

		valR.xy += mul_complex(imgR.xy,k.xy);
		valR.zw += mul_complex(imgR.zw,k.zw);

		valG.xy += mul_complex(imgG.xy,k.xy);
		valG.zw += mul_complex(imgG.zw,k.zw);

		valB.xy += mul_complex(imgB.xy,k.xy);
		valB.zw += mul_complex(imgB.zw,k.zw);
	}

	const vec4 w = vec4(0.411259,-0.548794,0.513282,4.561110);

	F = vec4(
		dot(valR,w),
		dot(valG,w),
		dot(valB,w),
		texture(bg_masked,var_uv).a );
}
