#version 300 es

precision lowp float;
precision lowp sampler2D;

in vec4 var_uv;

layout( location = 0 ) out vec4 F;

uniform sampler2D glfx_BG_MASK;
uniform sampler2D glfx_BACKGROUND;

void main()
{
	float mask = texture(glfx_BG_MASK,var_uv.zw).x;
	F = vec4( texture(glfx_BACKGROUND,var_uv.xy).xyz*mask, mask );
}
